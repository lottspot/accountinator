CC=gcc
CFLAGS=-Wall -lsqlite3

debug:clean
	$(CC) $(CFLAGS) -g -o accountinator main.c
stable:clean
	$(CC) $(CFLAGS) -o accountinator main.c
clean:
	rm -vfr *~ accountinator
