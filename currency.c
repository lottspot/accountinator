#include "currency.h"
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

long double get_conversion_factor(CURRENCY *from, CURRENCY *to) {
  return to->conversion_unit / from->conversion_unit;
}

CURRENCY *build_currency(char name[CURRENCY_NAME_SIZE], long double conversion_unit) {
  CURRENCY *c = malloc(sizeof(CURRENCY));
  if ( !c )
    return NULL;
  strncpy(c->name, name, CURRENCY_NAME_SIZE);
  c->conversion_unit = conversion_unit;
  c->storage_meta = NULL;
  return c;
}

void destroy_currency(CURRENCY **c) {
  if ( (*c)->storage_meta )
    free((*c)->storage_meta);
  free(*c);
  *c = NULL;
}