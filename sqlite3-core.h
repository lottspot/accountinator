#ifndef SQLITE3_CORE_H
#define SQLITE3_CORE_H
#include <sqlite3.h>
#include "currency.h"
#include "transaction.h"
#include "account.h"

typedef struct STORAGE_ERROR {
  char *msg;
  char *frame;
  struct STORAGE_ERROR* next;
} STORAGE_ERROR;

typedef struct {
  sqlite3 *storage;
  char *path;
  STORAGE_ERROR *read_errors;
  STORAGE_ERROR *write_errors;
} STORAGE;

typedef struct {
  long int id;
} STORAGE_META;

typedef struct {
  char *id;
  char name[3];
  char *conversion_unit;
} CURRENCY_RECORD;

typedef struct {
  char *id;
  char *amount;
  char *operation;
  char *currency_id;
  char *account_id;
  char *date;
  char *note;
} TRANSACTION_RECORD;

typedef struct {
  char *id;
  char *name;
  char *balance;
  char *currency_id;
} ACCOUNT_RECORD;

char *TABLES[] = {
  "currencies",
  "accounts",
  "transactions",
};

char *COLUMNS[] = {
  // currencies
  "id INTEGER PRIMARY KEY AUTOINCREMENT," \
  "name VARCHAR(3) UNIQUE NOT NULL," \
  "conversion_unit LONG DOUBLE NOT NULL",
  // accounts
  "id INTEGER PRIMARY KEY AUTOINCREMENT," \
  "name VARCHAR(20) UNIQUE NOT NULL," \
  "b alance FLOAT NOT NULL," \
  "currency INTEGER NOT NULL," \
  "FOREIGN KEY(currency) REFERENCES currencies(id)",
  // transactions
  "id INTEGER PRIMARY KEY AUTOINCREMENT," \
  "amount LONG DOUBLE NOT NULL," \
  "operation INTEGER NOT NULL," \
  "currency INTEGER NOT NULL," \
  "account INTEGER NOT NULL," \
  "time DATETIME NOT NULL," \
  "note TEXT," \
  "FOREIGN KEY(currency) REFERENCES currencies(id)," \
  "FOREIGN KEY(account) REFERENCES accounts(id)",
};

// Initiate a database connection, build a storage obejct around it
STORAGE* build_storage(char* path);
void destroy_storage(STORAGE **s);
int open_storage(STORAGE *s);
int close_storage(STORAGE *s);
// Link a new STORAGE_ERROR object to the front of list **list
void add_error_to_list(STORAGE_ERROR *e, STORAGE_ERROR **list);
// Return true if table exists in db, false oterwise
int table_exists(char *table, sqlite3 *db);
// Build DB tables
int build_tables(char* tables[], char *columns[], sqlite3* db);
// Take input buffers, format string, and build full SQL statement from them
char *format_query(const char *format_string,...);

#endif