#include <malloc.h>
#include <time.h>
#include "transaction.h"

TRANSACTION* build_transaction(tr_type operation, long double amount, CURRENCY *c, ACCOUNT *a, char *note) {
  TRANSACTION *t = malloc(sizeof(TRANSACTION));
  if ( !t )
    return NULL;
  if (amount < 0) {
    t->amount = -(amount);
    t->operation = DEBIT;
  } else {
    t->amount = amount;
    t->operation = operation;
  }
  t->currency = c;
  t->account = a;
  t->note = note;
  t->date = time(NULL);
  t->storage_meta = NULL;
  return t;
}

void destroy_transaction(TRANSACTION **t){
  if ( (*t)->note )
    free((*t)->note);
  if ( (*t)->storage_meta )
    free((*t)->storage_meta);
  free(*t);
  *t = NULL;
}

long double get_transaction_amount(TRANSACTION* t) {
  return t->amount * t->operation;
}