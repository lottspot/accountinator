#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <linux/limits.h>
#include "sqlite3-core.h"
#include "process-strings.h"
#define QUERY_BUFFER_SIZE 1024
#define CREATE_MODE 0755

/*
 * NON-PORTABLE INCLUDES:
 * linux/limits.h
 */

static int build_path(char path[PATH_MAX], char *save){
  const char *delim = "/";
  if ( save == NULL ) {
    // First run
    char init_path[PATH_MAX];
    char pathcpy[PATH_MAX];
    strncpy(pathcpy, path, PATH_MAX);
    init_path[0] = '\0';
    if( path[0] == *delim ) {
      // Path is absolute; add leading delimiter
      init_path[0] = *delim;
      init_path[1] = '\0';
    }
    // Get first path component, attempt directory build
    char *token = strtok_r(pathcpy, delim, &save);
    strncat(init_path, token, PATH_MAX);
    strncat(init_path, "/", PATH_MAX);
    if ( mkdir(init_path, CREATE_MODE) < 0 ) {
      // There was an error, find out what
      switch ( errno ) {
	case EEXIST:
	  // Directory exists; that's ok, keep building
	  return build_path(init_path, save);
	case EACCES:
	  // Insufficient permission; bail out
	  return errno;
	default:
	  // Unhandled error, return generic non-success
	  return -1;
      }
    } else {
      // No errors, keep building
      return build_path(init_path, save);
    }
  }
  // Subsequent runs
  char *token = strtok_r(NULL, delim, &save);
  if ( !token )
    // End of path; we made it
    return 0;
  // Prepare and attempt next directory build
  strncat(path, token, PATH_MAX);
  if ( mkdir(path, CREATE_MODE ) < 0 ) {
    // There was an error; find out what
    switch ( errno ) {
      case EEXIST:
	// Directory already exists; that's ok, keep building
	return build_path(path, save);
      case EACCES:
	// Insufficient permission; bail out
	return errno;
      default:
	// Unhandled error; bail with generic non-success
	return -1;
    }
  } else {
    // No errors, keep building
    return build_path(path, save);
  }
}

static int separate_file_from_path(char in_path[PATH_MAX], char out_path[PATH_MAX], char out_file[PATH_MAX]){
  // Initialize
  const char *delim = "/";
  char *token = NULL;
  char *last_token = NULL;
  char in_pathcpy[PATH_MAX];
  strncpy(in_pathcpy, in_path, PATH_MAX);
  out_path[0] = '\0';
  if ( in_path[0] == *delim ) {
    // Path is absolute; add leading delimiter
    out_path[0] = *delim;
    out_path[1] = '\0';
  }
  // Get first token, save with save pointer
  token = strtok(in_pathcpy, delim);
  if ( token == NULL )
    return 0;
  last_token = token;
  while ( (token = strtok(NULL, delim)) != NULL ) {
    /* Another token was extracted 
     * Add last saved token to path, save extracted token
     */
    strncat(out_path, last_token, PATH_MAX);
    strncat(out_path, delim, PATH_MAX);
    last_token = token;
  }
  // No more tokens; last saved token must be filename
  if ( out_file != NULL )
    strncpy(out_file, last_token, PATH_MAX);
  return 1;
}

void add_error_to_list(STORAGE_ERROR *e, STORAGE_ERROR **list){
  if ( *list ) {
    // List has errors; add to start of list
    e->next = (*list)->next;
    *list = e;
  } else {
    // First error on list; set as first item
    *list = e;
  }
}

int table_exists(char *table, sqlite3 *db) {
  // Initialize
  size_t bufSize = 20;
  char *sql = sqlite3_malloc(bufSize);
  if ( !sql )
    return 0;
  // If the buffer is completely filled by snprintf
  while ( strlen( sqlite3_snprintf(bufSize, sql, "SELECT NULL FROM %s", table) ) == bufSize -1 ) {
    // Increase the buffer size
    bufSize *= 2;
    sql = sqlite3_realloc(sql, bufSize);
    if ( !sql )
      return 0;
  }
  // Check for the table
  int result = sqlite3_exec(db, sql, NULL, NULL, NULL);
  sqlite3_free(sql);
  // If no errors, the table exists.
  if ( result == SQLITE_OK ) {
    return 1;
  } else {
    return 0;
  }
}

char *format_query(const char *format_string,...) {
  // Initialize
  size_t bufSize = QUERY_BUFFER_SIZE;
  char *sql = sqlite3_malloc(bufSize);
  if ( sql == NULL ) {
    return NULL;
  }
  va_list arg_list;
  va_start(arg_list, format_string);
  // Do work
  while (
    // If vsnprintf fills buffer
    strlen(
      sqlite3_vsnprintf(
	bufSize,
	sql,
	format_string,
	arg_list
      )
    ) == bufSize - 1
  ) {
    // Increase buffer size
    bufSize *= 2;
    sql = sqlite3_realloc(sql, bufSize);
    if ( sql == NULL )
      return NULL;
    // Reset the varargs list (SEGFAULT if this is not done)
    va_start(arg_list, format_string);
  }
  va_end(arg_list);
  return sql;
}

int build_tables(char* tables[], char* columns[], sqlite3* db) {
  // Initialize
  int retVal = SQLITE_OK; // Unless something goes wrong, we assume OK
  int result;
  int i;
  char *sql = NULL;
  // Do work
  for ( i=0; tables[i]; i++ ) {
    if ( !table_exists(tables[i], db) )
      // Table does not exist; build a query to create it
      if ( ! (sql = format_query("CREATE TABLE %s(%s);", tables[i], columns[i])) )
	return -1;
      // Create table
      result = sqlite3_exec(db, sql, NULL, NULL, NULL);
      sqlite3_free(sql);
      if ( result != SQLITE_OK ) {
	/* Error building table
	 * Set return value to loop iteration which failed
	 * Bail out
	 */
	retVal = ++i;
	break;
      }
  }
  return retVal;
}

STORAGE* build_storage(char* path) {
  STORAGE *out = malloc(sizeof(STORAGE));
  out->path = path;
  out->storage = NULL;
  out->read_errors = NULL;
  out->write_errors = NULL;
  return out;
}

static void destroy_error_list(STORAGE_ERROR **e) {
  STORAGE_ERROR *last = NULL;
  while ( *e ) {
    last = *e;
    *e = (*e)->next;
    free(last);
  }
}

void destroy_storage(STORAGE **s) {
  if ( (*s)->storage )
    close_storage(*s);
  if ( (*s)->path )
    free((*s)->path);
  if ( (*s)->read_errors )
    destroy_error_list(&((*s)->read_errors));
  if ( (*s)->write_errors )
    destroy_error_list(&((*s)->write_errors));
  free(*s);
  *s = NULL;
}

int open_storage(STORAGE *s){
  char dir[PATH_MAX];
  if ( !separate_file_from_path(s->path, dir, NULL) )
    return -1;
  if ( build_path(dir, NULL) != 0 )
    return -2;
  if ( sqlite3_open_v2(s->path, &(s->storage), (SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE), "unix-excl") != SQLITE_OK ) {
    if ( s->storage )
      sqlite3_close_v2(s->storage);
    s->storage = NULL;
    return -3;
  }
  return build_tables(TABLES, COLUMNS, s->storage);
}

int close_storage(STORAGE* s) {
  if ( ! s->storage )
    return -1;
  if ( sqlite3_close_v2(s->storage) == SQLITE_OK )
    return 0;
  return -1;
}
