#include <malloc.h>
#include "account.h"

ACCOUNT* build_account(char* name, long double balance, CURRENCY* c) {
  ACCOUNT *a = malloc(sizeof(ACCOUNT));
  if ( !a )
    return NULL;
  a->balance = balance;
  a->currency = c;
  a->name = name;
  a->storage_meta = NULL;
  return a;
}

void destroy_account(ACCOUNT **a) {
  if ( (*a)->name )
    free((*a)->name);
  if ( (*a)->storage_meta )
    free((*a)->storage_meta);
  free(*a);
  *a = NULL;
}