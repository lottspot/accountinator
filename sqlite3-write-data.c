#include <malloc.h>
#include <string.h>
#include <stdarg.h>
#include "sqlite3-write-data.h"
#include "process-strings.h"

static void cast_id_from_meta(TYPED_DATA *id, void *storage_meta){
  if ( !storage_meta ) {
    id->type = UNDEF;
  } else {
    STORAGE_META *meta = (STORAGE_META*)storage_meta;
    id->data.li = meta->id;
    id->type = LONG_INTEGER;
  }
}

CURRENCY_RECORD* currency_to_record(CURRENCY* c) {
  TYPED_DATA conversion_unit;
  TYPED_DATA id;
  CURRENCY_RECORD *record = malloc(sizeof(CURRENCY_RECORD));
  if ( !record )
    return NULL;
  // Copy all struct members into TYPED_DATA objects
  cast_id_from_meta(&id, c->storage_meta);
  conversion_unit.data.ld = c->conversion_unit;
  conversion_unit.type = LONG_DOUBLE;
  if ( 
    // If any input data fails to convert to string type
    convert_to_string(
      2,
      &conversion_unit,
      &id
    ) != 0
  ) {
    // free() all malloc()'d memory and bail out
    free_typed_strings(
      2,
      &conversion_unit,
      &id
    );
    free(record);
    return NULL;
  }
  // On success, populate record with converted input data
  record->id = id.data.c;
  record->conversion_unit = conversion_unit.data.c;
  strncpy(record->name, c->name, CURRENCY_NAME_SIZE);
  return record;
}

ACCOUNT_RECORD* account_to_record(ACCOUNT* a) {
  TYPED_DATA id;
  TYPED_DATA currency_id;
  TYPED_DATA balance;
  ACCOUNT_RECORD *record = malloc(sizeof(ACCOUNT_RECORD));
  if ( !record )
    return NULL;
  // Copy all struct members into TYPED_DATA objects
  cast_id_from_meta(&id, a->storage_meta);
  cast_id_from_meta(&currency_id, a->currency->storage_meta);
  balance.data.ld = a->balance;
  balance.type = LONG_DOUBLE;
  if ( 
    // If any input data fails to convert to string type
    convert_to_string(
	  3,
	  &id,
	  &currency_id,
	  &balance
	) != 0
  ) {
    // free() all malloc()'d memory and bail out
    free_typed_strings(
      3,
      &id,
      &currency_id,
      &balance
    );
    free(record);
    return NULL;
  }
  // On success, populate record with converted input data
  record->id = id.data.c;
  record->currency_id = currency_id.data.c;
  record->balance = balance.data.c;
  record->name = a->name;
  return record;
}

TRANSACTION_RECORD* transaction_to_record(TRANSACTION* t) {
  TYPED_DATA id;
  TYPED_DATA currency_id;
  TYPED_DATA account_id;
  TYPED_DATA operation;
  TYPED_DATA amount;
  TYPED_DATA date;
  TRANSACTION_RECORD *record = malloc(sizeof(TRANSACTION_RECORD));
  if ( !record )
    return NULL;
  // Copy all struct members into TYPED_DATA objects
  cast_id_from_meta(&id, t->storage_meta);
  cast_id_from_meta(&currency_id, t->currency->storage_meta);
  cast_id_from_meta(&account_id, t->account->storage_meta);
  operation.data.i = t->operation;
  operation.type = INTEGER;
  amount.data.ld = t->amount;
  amount.type = LONG_DOUBLE;
  date.data.t = t->date;
  date.type = TIME_T;
  if (
    // If any input data fails to convert to string type
    convert_to_string(
      6,
      &id,
      &currency_id,
      &account_id,
      &operation,
      &amount,
      &date
    ) != 0
  ) {
    // free() all malloc()'d memory and bail out
    free_typed_strings(
      6,
      &id,
      &currency_id,
      &account_id,
      &operation,
      &amount,
      &date
    );
    free(record);
    return NULL;
  }
  // On success, populate record with converted input data
  record->id = id.data.c;
  record->currency_id = currency_id.data.c;
  record->account_id = account_id.data.c;
  record->operation = operation.data.c;
  record->amount = amount.data.c;
  record->date = date.data.c;
  record->note = t->note;
  return record;
}

void destroy_currency_record(CURRENCY_RECORD **c) {
  if ( (*c)->conversion_unit )
    free((*c)->conversion_unit);
  if ( (*c)->id )
    free((*c)->id);
  free(*c);
  *c = NULL;
}

void destroy_account_record(ACCOUNT_RECORD **a) {
  if ( (*a)->balance )
    free((*a)->balance);
  if ( (*a)->currency_id )
    free((*a)->currency_id);
  if ( (*a)->id )
    free((*a)->id);
  free(*a);
  *a = NULL;
}

void destroy_transaction_record(TRANSACTION_RECORD **t) {
  if ( (*t)->account_id )
    free((*t)->account_id);
  if ( (*t)->amount )
    free((*t)->amount);
  if ( (*t)->currency_id )
    free((*t)->currency_id);
  if ( (*t)->date )
    free((*t)->date);
  if ( (*t)->id )
    free((*t)->id);
  if ( (*t)->operation )
    free((*t)->operation);
  free(*t);
  *t = NULL;
}

static long int finalize_write(char **sql, STORAGE *s, void **object_meta) {
  // If we didn't get the input we need, bail out
  if ( !(*sql && s) )
    return 0;
  // Initialize data
  int result;
  char* err_msg = NULL;
  STORAGE_META *meta = malloc(sizeof(STORAGE_META));
  if ( !meta ) {
    sqlite3_free(*sql);
    return 0;
  }
  STORAGE_ERROR *err = malloc(sizeof(STORAGE_ERROR));
  if (! err )
    return 0;
  err->frame = "finalize_write";
  err->next = NULL;
  // Do work
  result = sqlite3_exec(s->storage, *sql, NULL, NULL, &err_msg);
  meta->id = sqlite3_last_insert_rowid(s->storage);
  sqlite3_free(*sql);
  *sql = NULL;
  if ( result == SQLITE_OK ){
    // On successful write, finish up
    free(err);
    *object_meta = meta;
    return meta->id;
  } else {
    // cleanup
    free(meta);
    err->msg = err_msg;
    // Record error
    add_error_to_list(err, &(s->write_errors));
    // Determine xstat and bail
    switch ( result ) {
      case SQLITE_CONSTRAINT:
         return -1;
       default:
         return 0;
    }
  }
}

long int write_new_currency(CURRENCY* c, STORAGE *store) {
  // Initialize
  const char *sql_format = "INSERT INTO currencies(name, conversion_unit) " \
			"VALUES(%Q, %Q)";
  CURRENCY_RECORD *record = currency_to_record(c);
  if ( !record )
    return 0;
  // Build a query
  char *sql = format_query(
    sql_format, 
    record->name, 
    record->conversion_unit
  );
  // Cleanup record and execute query
  destroy_currency_record(&record);
  return finalize_write(&sql, store, &(c->storage_meta));
}

long int write_new_account(ACCOUNT *a, STORAGE* store) {
  // Write to storage any unstored objects which are associated
  if ( !a->currency->storage_meta )
    if ( !write_new_currency(a->currency, store) )
      return 0;
  // Initialize
  const char *sql_format = "INSERT INTO " \
			  "accounts(name, balance, currency)" \
			  "VALUES(%Q, %Q, %Q)";
  ACCOUNT_RECORD *record = account_to_record(a);
  if ( !record )
    return 0;
  // Build a query
  char *sql = format_query(
    sql_format,
    record->name,
    record->balance,
    record->currency_id
  );
  // Cleanup record and execute query
  destroy_account_record(&record);
  return finalize_write(&sql, store, &(a->storage_meta));
}

long int write_new_transaction(TRANSACTION* t, STORAGE* store) {
  // Write to storage any unstored objects which are associated
  if ( !t->currency->storage_meta )
    if ( !write_new_currency(t->currency, store) )
      return 0;
  if ( !t->account->storage_meta )
    if ( !write_new_account(t->account, store) )
      return 0;
  // Initialize
  const char *sql_format = "INSERT INTO " \
		        "transactions(amount, operation, currency, account, time, note) " \
			"VALUES(%Q, %Q, %Q, %Q, %Q, %Q)";
  TRANSACTION_RECORD *record = transaction_to_record(t);
  if ( !record )
    return 0;
  // Build a query
  char *sql = format_query(
    sql_format,
    record->amount,
    record->operation,
    record->currency_id,
    record->account_id,
    record->date,
    record->note
  );
  // Cleanup record and execute query
  destroy_transaction_record(&record);
  return finalize_write(&sql, store, &(t->storage_meta));
}