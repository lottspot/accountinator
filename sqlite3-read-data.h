#ifndef READ_DATA_H
#define READ_DATA_H
#include "sqlite3-core.h"
#include "account.h"

CURRENCY *read_currencies(void);
ACCOUNT *read_account(void);
TRANSACTION *read_transactions(void);

#endif