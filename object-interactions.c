#include "object-interactions.h"

void convert_account_to_currency(ACCOUNT *a, CURRENCY *c, long double (*get_factor)(CURRENCY* from, CURRENCY *to)) {
  long double old2new = get_factor(a->currency, c);
  a->balance = a->balance * old2new;
  a->currency = c;
}

void convert_transaction_to_currency(TRANSACTION* t, CURRENCY* c, long double (*get_factor)(CURRENCY* from, CURRENCY *to)) {
  long double old2new = get_factor(t->currency, c);
  t->amount = t->amount * old2new;
  t->currency = c;
}

int apply_transaction_to_account(TRANSACTION* t, ACCOUNT* a) {
  if ((*a->currency).name != (*t->currency).name) {
    return 0;
  }
  a->balance += get_transaction_amount(t);
  return 1;
}