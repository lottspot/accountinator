#ifndef OBJECT_INTERACTIONS_H
#define OBJECT_INTERACTIONS_H
#include "currency.h"
#include "transaction.h"
#include "account.h"

// Converts balance of an account from one currency type to equivalent amount of another
void convert_account_to_currency(ACCOUNT *a, CURRENCY *c, long double (*get_factor)(CURRENCY *from, CURRENCY *to));
// Converts a transaction amount from one currency type to equivalent amount of another
void convert_transaction_to_currency(TRANSACTION* t, CURRENCY* c, long double (*get_factor)(CURRENCY* from, CURRENCY *to));
// Alters account balance by applying a transaction
int apply_transaction_to_account(TRANSACTION *t, ACCOUNT *a);

#endif