#ifndef TRANSACTION_H
#define TRANSACTION_H
#include <time.h>
#include "currency.h"
#include "account.h"

typedef enum { 
  DEBIT = -1,
  CREDIT = 1
} tr_type;

typedef struct {
  CURRENCY *currency;
  ACCOUNT *account;
  tr_type operation;
  long double amount;
  /* NOTE: the function
   * time_t time(time_t *time) 
   * from time.h will set the 'time' argument
   * equal to current system time
   *
   * NOTE: The function
   * char *ctime(time_t *time) 
   * from time.h can convert time value 
   * to human readable (string) format
   */
  time_t date;
  char *note;
  void *storage_meta;
} TRANSACTION;

// Constructor function for transaction type
TRANSACTION *build_transaction(tr_type operation, long double amount, CURRENCY *c, ACCOUNT *a, char *note);
// Destructor function for transaction type
void destroy_transaction(TRANSACTION **t);
// Returns the net amount of a transaction type
long double get_transaction_amount(TRANSACTION *t);

#endif