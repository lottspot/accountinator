#ifndef WRITE_DATA_H
#define WRITE_DATA_H
#include "account.h"
#include "sqlite3-core.h"

// Create a copy of the given object as a record object
CURRENCY_RECORD *currency_to_record(CURRENCY *c);
ACCOUNT_RECORD *account_to_record(ACCOUNT *a);
TRANSACTION_RECORD *transaction_to_record(TRANSACTION *t);
// Destructor functions for record objects
void destroy_currency_record(CURRENCY_RECORD **c);
void destroy_account_record(ACCOUNT_RECORD **a);
void destroy_transaction_record(TRANSACTION_RECORD **t);
// Write to storage an object which is not yet stored
long int write_new_currency(CURRENCY *c, STORAGE *store);
long int write_new_account(ACCOUNT *a, STORAGE *store);
long int write_new_transaction(TRANSACTION *t, STORAGE *store);

#endif