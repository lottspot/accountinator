#ifndef ACCOUNT_H
#define ACCOUNT_H
#include "currency.h"

typedef struct {
  char *name;
  long double balance;
  CURRENCY *currency;
  void* storage_meta;
} ACCOUNT;

// Constructor function for account type
ACCOUNT *build_account(char *name, long double balance, CURRENCY *c);
// Destructor function for account type
void destroy_account(ACCOUNT **a);

#endif