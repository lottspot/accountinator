#ifndef CURRENCY_H
#define CURRENCY_H
#define CURRENCY_NAME_SIZE 4

typedef struct {
  char name[CURRENCY_NAME_SIZE];
  long double conversion_unit;
  void *storage_meta;
} CURRENCY;

// Constructor function for currency type
CURRENCY *build_currency(char name[CURRENCY_NAME_SIZE], long double conversion_unit);
// Destructor function for currency type
void destroy_currency(CURRENCY **c);
/*
 * Returns multiplier used to convert an amount in
 * one currency to the equivalent amount in another
 */
long double get_conversion_factor(CURRENCY *from, CURRENCY *to);

#endif