#include <string.h>
#include <malloc.h>
#include <stdarg.h>
#include "process-strings.h"
#ifndef BUF_MULTIPLIER
#define BUF_MULTIPLIER 2
#endif
#ifndef BUF_INCREMENTER
#define BUF_INCREMENTER 10
#endif

static int get_snprintf_rval(int snprintf_result, size_t sBuf) {
  // If the length of the input string was less then the buffer size 
  if ( (int)( snprintf_result - sBuf ) < 0  ){
    // Tell the caller we're good to go
    return 1;
  // If the length of the input string was less than the value of the incrementer
  } else if ( (int)(snprintf_result - sBuf) < BUF_INCREMENTER  ) {
    // Tell the caller to increment the buffer size
    return -1;
  } else {
    // Otherwise, tell the caller we're not even close
    return 0;
  }
}

static int ldtoa (void *in, char *zBuf, size_t sBuf) {
  long double value = *(long double*)in;
  int result = snprintf(zBuf, sBuf, "%Lf", value);
  return get_snprintf_rval(result, sBuf);
}

static int itoa (void *in, char *zBuf, size_t sBuf) {
  int value = *(int*)in;
  int result = snprintf(zBuf, sBuf, "%d", value);
  return get_snprintf_rval(result, sBuf);
}

static int utoa (void *in, char *zBuf, size_t sBuf) {
  unsigned int value = *(unsigned int*)in;
  int result = snprintf(zBuf, sBuf, "%u", value);
  return get_snprintf_rval(result, sBuf);
}

static int litoa (void *in, char*zBuf, size_t sBuf) {
  long int value = *(long int*)in;
  int result = snprintf(zBuf, sBuf, "%ld", value);
  return get_snprintf_rval(result, sBuf);
}

static int timetoa (void *in, char *zBuf, size_t sBuf) {
  time_t *value = (time_t*)in;
  return strftime(zBuf, sBuf, "%Y-%m-%d %H:%M", localtime(value));
}

static int multiply_buf_size(char **zBuf, size_t *sBuf, int multiplier) {
  *sBuf *= multiplier;
  *zBuf = realloc(*zBuf, *sBuf);
  if ( *zBuf ) {
    return 1;
  } else {
    return 0;
  }
}

static int increment_buf_size(char **zBuf, size_t *sBuf, int incrementer) {
  *sBuf += incrementer;
  *zBuf = realloc(*zBuf, *sBuf);
  if ( *zBuf ) {
    return 1;
  } else {
    return 0;
  }
}

static int perform_conversion(TYPED_DATA *d, void *callback_arg, int (*callback)(void *callback_arg, char *zBuf, size_t sBuf)){
  // Initialize
  int result;
  size_t sBuf = 2;
  char *buf = NULL;
  if ( ! (buf = malloc(sBuf)) )
    return 0;
  // Until the callback tells us we successfully wrote to the buffer
  while ( (result = callback(callback_arg, buf, sBuf)) < 1 ) {
    // Figure out how far we were off
    switch (result) {
      // We weren't even close; double the buffer size
      case 0:
        if (! multiply_buf_size(&buf, &sBuf, BUF_MULTIPLIER) ) {
	  return 0;
        }
        break;
      // We were pretty close. Increase by BUF_INCREMENTER
      case -1:
        if (! increment_buf_size(&buf, &sBuf, BUF_INCREMENTER) ) {
	  return 0;
        }
        break;
      // Unknown return code; bail out
      default:
	free(buf);
        return 0;
    }
  }
  // Successfully wrote to buffer; finish up
  d->data.c = buf;
  d->type = CHARACTER;
  return 1;
}



int convert_to_string(int argc,...) {
  // Initialize
  int i;
  va_list args;
  TYPED_DATA *d = NULL;
  va_start(args, argc);
  // Iterate through the varargs list
  for ( i = 0; i < argc; i++ ) {
    d = va_arg(args, TYPED_DATA*);
    // Perform conversion with appropriate callback for data type
    switch(d->type) {
	case UNDEF:
	  d->data.c = NULL;
	  break;
        case INTEGER:
	  if (!  perform_conversion(d, &(d->data.i), itoa) )
	    return ++i;
	  break;
	case LONG_INTEGER:
	  if (!  perform_conversion(d, &(d->data.li), litoa) )
	    return ++i;
	  break;
        case UNSIGNED_INTEGER:
	  if (!  perform_conversion(d, &(d->data.ui), utoa) )
	    return ++i;
	  break;
        case LONG_DOUBLE:
	  if (!  perform_conversion(d, &(d->data.ld), ldtoa) )
	    return ++i;
	  break;
        case TIME_T:
	  if (!  perform_conversion(d, &(d->data.t), timetoa) )
	    return ++i;
	  break;
	// Unrecognized data type; bail out
        default:
          return -1;
      }
  }
  // All conversions good; return success status
  return 0;
}

void free_typed_strings(int argc,...) {
  int i;
  va_list args;
  TYPED_DATA *d = NULL;
  va_start(args, argc);
  for ( i = 0; i < argc; i++ ) {
    d = va_arg(args, TYPED_DATA*);
    if ( d->type == CHARACTER && d->data.c != NULL ) {
      free(d->data.c);
    }
  }
}