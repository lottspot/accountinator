#ifndef PROCCESS_STRINGS_H
#define PROCCESS_STRINGS_H
#include <time.h>

typedef struct {
  enum {
    UNDEF = 0,
    CHARACTER,
    INTEGER,
    FLOAT,
    DOUBLE,
    LONG_INTEGER,
    LONG_DOUBLE,
    UNSIGNED_INTEGER,
    UL_INTEGER,
    TIME_T
  } type;
  
  union {
    char *c;
    int i;
    float f;
    double d;
    long int li;
    long double ld;
    unsigned int ui;
    unsigned long int uli;
    time_t t;
  } data;
} TYPED_DATA;

// Given any number of TYPED_DATA structs, convert their data to string type
int convert_to_string(int argc,...);
// Given any number of TYPED_DATA struct of type STRING, free their buffer memory
void free_typed_strings(int argc,...);

#endif